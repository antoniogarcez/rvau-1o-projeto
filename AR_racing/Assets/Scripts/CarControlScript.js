﻿#pragma strict

var Vertical : String;
var Horizontal : String;

var wheelFL : WheelCollider;
var wheelFR : WheelCollider;
var wheelRL : WheelCollider;
var wheelRR : WheelCollider;

var wheelFLTrans : Transform;
var wheelFRTrans : Transform;
var wheelRLTrans : Transform;
var wheelRRTrans : Transform;

private var lowestSteerAtSpeed : float;
private var lowSpeedSteerAngel : float = 30;
private var highSpeedSteerAngel : float = 1;

var no_acelerationSpeed : float = 40;
var breakMovement : float = 150;

var maxTorque : float = 30;
var currentSpeed : float;
var topSpeed : float = 250;
var maxReverseSpeed : float = 50;

var tracks : GameObject;
var RaceReady : GameObject;

var showKilometers : GUIText;

var F_Car : GUIText;
var R_Car : GUIText;

var color_gray : float = 124/255;

function Start () 
{
	rigidbody.centerOfMass.y = 0;
	
}

// FixedUpdate pode ser chamada varias vezes por frame, o que nao acontece com o Update (uma vez por frame)
//function FixedUpdate () 
function Update()
{
	Debug.Log("RaceReady: "+RaceReady.active);
	if(RaceReady.active){
		//this.gameObject.SetActive(true);
	}else{
		currentSpeed = 0;
		//this.gameObject.SetActive(false);
		return;
	}
	currentSpeed = 2*22/7*wheelRL.radius*wheelRL.rpm*60/1000; // Velocidade em quilometros por hora
	lowestSteerAtSpeed = 500 - currentSpeed;
	
	if(currentSpeed <= topSpeed && currentSpeed >= -maxReverseSpeed)
	{
		wheelRR.motorTorque = maxTorque * Input.GetAxis(Vertical);
		wheelRL.motorTorque = maxTorque * Input.GetAxis(Vertical);
	}
	else
	{
		wheelRR.motorTorque = 0;
		wheelRL.motorTorque = 0;
	}

	currentSpeed = Mathf.Round(currentSpeed);
	
	if(Input.GetButton(Vertical) == false)
	{
		wheelRR.brakeTorque = no_acelerationSpeed;
		wheelRL.brakeTorque = no_acelerationSpeed;
	}
	else
	{
		if(Input.GetAxis(Vertical) < 0 && currentSpeed > 0)
		{
			wheelRR.brakeTorque = breakMovement;
			wheelRL.brakeTorque = breakMovement;
		}
		else
		{
			wheelRR.brakeTorque = 0;
			wheelRL.brakeTorque = 0;
		}
	}
	
	var speedFactor = rigidbody.velocity.magnitude / lowestSteerAtSpeed;
	var currentSteerAngel = Mathf.Lerp(lowSpeedSteerAngel, highSpeedSteerAngel, speedFactor);
	
	//print(currentSteerAngel);
	currentSteerAngel *= Input.GetAxis(Horizontal);
	
	wheelFL.steerAngle = currentSteerAngel;
	wheelFR.steerAngle = currentSteerAngel;
/*
}

function Update()
{
*/
	if(tracks.renderer.enabled){
		//print("active");
		this.transform.rigidbody.useGravity = true;
	}else{
		//print("disable");
		this.transform.rigidbody.useGravity = false;
	}
	// Rodar as rodas em torno de si proprio
	wheelFLTrans.Rotate(wheelFL.rpm/60*360*Time.deltaTime, 0, 0);
	wheelFRTrans.Rotate(wheelFR.rpm/60*360*Time.deltaTime, 0, 0);
	wheelRLTrans.Rotate(wheelRL.rpm/60*360*Time.deltaTime, 0, 0);
	wheelRRTrans.Rotate(wheelRR.rpm/60*360*Time.deltaTime, 0, 0);
	
	//Rodar rodas
	wheelFLTrans.localEulerAngles.y = wheelFL.steerAngle - wheelFLTrans.localEulerAngles.z;
	wheelFRTrans.localEulerAngles.y = wheelFR.steerAngle - wheelFRTrans.localEulerAngles.z;
	
	putSpeedOnScreen();
	
	if(currentSpeed > 0)
	{
		F_Car.color = Color(255, 178, 0);
		R_Car.color = Color(color_gray, color_gray, color_gray);
	}
	else if (currentSpeed < 0)
	{
		F_Car.color = Color(color_gray, color_gray, color_gray);
		R_Car.color = Color(255, 178, 0);
	}
	else if(currentSpeed == 0)
	{
		F_Car.color = Color(color_gray, color_gray, color_gray);
		R_Car.color = Color(color_gray, color_gray, color_gray);
	}
}


function putSpeedOnScreen()
{	
	if(currentSpeed >= 0)
	{
		if(currentSpeed >= 0 && currentSpeed < 10)
		{
			showKilometers.text = "00" + currentSpeed.ToString();
		}
		else if(currentSpeed >= 10 && currentSpeed < 100)
		{
			showKilometers.text = "0" + currentSpeed.ToString();
		}
		else
		{
			showKilometers.text = currentSpeed.ToString();
		}
	}
	else if(currentSpeed < 0)
	{
		if(Mathf.Abs(currentSpeed) > 0 && Mathf.Abs(currentSpeed) < 10)
		{
			showKilometers.text = "0" + (Mathf.Abs(currentSpeed)).ToString();
		}
		else if(Mathf.Abs(currentSpeed) >= 10 && Mathf.Abs(currentSpeed) <= 50)
		{
			showKilometers.text = (Mathf.Abs(currentSpeed)).ToString();
		}
	}
}