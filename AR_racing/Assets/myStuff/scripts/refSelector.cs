﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class refSelector : MonoBehaviour {
	
	private bool ReadyForRace = false;
	private bool RaceEnded = false;
	
	public GameObject cronometro;
	
	public GameObject[] myCars;
	
	public GameObject RaceReady;

	public GameObject next;
	public GameObject ok;
	public GameObject undo;
	
	private bool showingButton = false;

	public GameObject[] selectors;
	public GameObject tracks;
	public GameObject initTracks;
	public GameObject finalObjs;
	public GameObject[] objs;

	public GameObject textAskPlay;
	public GameObject[] textCountdown;

	public GameObject showBlueWinner;
	public GameObject showRedWinner;
	
	public GameObject showFinalTime;

	public GameObject textRestartRace;
	public GameObject textNewTrack;
	public GameObject textExit;

	public GameObject showKilometersBlue;
	public GameObject showKilometersRed;

	public GameObject F_Blue;
	public GameObject R_Blue;
	public GameObject F_Red;
	public GameObject R_Red;
	
	private int currentObj = 0;
	private List<GameObject> addedObjs = new List<GameObject>();
	private bool showTextAskPlay = false;
	private int countdownInt = 0;
	private bool countdownBool = false;
	
	private cronometroScript cronometrS;
	
	private List<Vector3> carsInitPosition = new List<Vector3>(); 
	private List<Quaternion> carsInitRotation = new List<Quaternion>();
	
	public bool getReady(){
		return ReadyForRace;
	}
	
	// Use this for initialization
	void Start () 
	{
		RaceReady.SetActive(false);
		
		cronometrS = cronometro.GetComponent<cronometroScript>();
		
		foreach(GameObject o in myCars)
		{
			o.SetActive(false);
			o.GetComponent<MonoBehaviour>().enabled = false;
		}
		foreach(GameObject o in objs)
		{
			o.SetActive(false);
		}
		objs[currentObj].SetActive(true);
		
		finalObjs.SetActive(false);

		textAskPlay.SetActive(false);

		foreach(GameObject countdown in textCountdown)
		{
			countdown.SetActive(false);
		}

		showKilometersBlue.SetActive(false);
		showKilometersRed.SetActive(false);

		showBlueWinner.SetActive(false);
		showRedWinner.SetActive(false);

		showFinalTime.SetActive(false);

		textRestartRace.SetActive(false);
		textNewTrack.SetActive(false);
		textExit.SetActive(false);

		F_Blue.SetActive(false);
		R_Blue.SetActive(false);
		F_Red.SetActive(false);
		R_Red.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		GameObject selector = selectors[0];
		int currentObj_oux = 0;
		if(selectors[1].renderer.enabled){
			selector = selectors[1];
			currentObj_oux = 1;
		}else if(selectors[2].renderer.enabled){
			selector = selectors[2];
			currentObj_oux = 2;
		}
		if(currentObj_oux != currentObj){
			currentObj = currentObj_oux;
			foreach(GameObject obj in objs){
				obj.SetActive(false);
			}
			objs[currentObj].SetActive(true);
		}
		
		if(!ReadyForRace){
			
			if(!showingButton){
				if(undo.renderer.enabled){
					showingButton = true;
					undoAction();
				}
			}else{
				if(!ok.renderer.enabled && !undo.renderer.enabled){
					showingButton = false;
				}
			}
			
		}
		if(RaceEnded){
			if(!showingButton){
				
				if(ok.renderer.enabled){
					showingButton = true;
					okAction();
				}else if(undo.renderer.enabled){
					showingButton = true;
					undoAction();
				}else if(selectors[2].renderer.enabled){// marca finish
					Application.LoadLevel(0);
				}
			}else{
				if(!ok.renderer.enabled && !undo.renderer.enabled){
					showingButton = false;
				}
			}
		}else
		if(selector.renderer.enabled && !ReadyForRace){
			finalObjs.SetActive(true);
			finalObjs.transform.position = new Vector3(selector.transform.parent.position.x,
											 finalObjs.transform.position.y,
			                                 selector.transform.parent.position.z);
			
			         
			finalObjs.transform.Rotate (0f,selector.transform.parent.localEulerAngles.y-
			                            finalObjs.transform.localEulerAngles.y,0f);
			
			if(!showingButton){
				
				if(ok.renderer.enabled){
					showingButton = true;
					okAction();
				}
			}else{
				if(!ok.renderer.enabled && !undo.renderer.enabled){
					showingButton = false;
				}
			}
			
		}/*else if(!ReadyForRace){
		
			if(!showingButton){
				if(undo.renderer.enabled){
					showingButton = true;
					undoAction();
				}
			}else{
				if(!ok.renderer.enabled && !undo.renderer.enabled){
					showingButton = false;
				}
			}
		
		}*/else if(ReadyForRace && showTextAskPlay)
		{
			if(!showingButton)
			{
				if(ok.renderer.enabled)
				{
					showingButton = true;
					okAction();
				}
				else if(undo.renderer.enabled)
				{
					showingButton = true;
					undoAction();
				}
			}
			else
			{
				if(!ok.renderer.enabled && !undo.renderer.enabled)
				{
					showingButton = false;
				}
			}
		}
		else if(ReadyForRace && countdownBool)
		{
			countdownInt++;

			if(countdownInt > 0 && countdownInt < 50)
			{
				textCountdown[0].SetActive(true);
			}
			else if(countdownInt > 50 && countdownInt < 100)
			{
				textCountdown[0].SetActive(false);
				textCountdown[1].SetActive(true);
			}
			else if(countdownInt > 100 && countdownInt < 150)
			{
				textCountdown[1].SetActive(false);
				textCountdown[2].SetActive(true);
			}
			else if (countdownInt > 150 && countdownInt < 200)
			{
				textCountdown[2].SetActive(false);
				textCountdown[3].SetActive(true);

				carsInitPosition.Clear();
				carsInitRotation.Clear();
				foreach(GameObject o in myCars)
				{
					o.GetComponent<MonoBehaviour>().enabled = true;
					carsInitPosition.Add(o.transform.position);
					carsInitRotation.Add(o.transform.rotation);
				}
				
				cronometrS.restartTime();
			}
			else if (countdownInt > 200 && countdownInt < 230)
			{
				textCountdown[3].SetActive(false);
				countdownInt = 0;
				countdownBool = false;
			}
		}
		else{
			finalObjs.SetActive(false);
		}
	}
	
	void nextAction(){
		currentObj++;
		if(currentObj >= objs.Length){
			currentObj = 0;
		}
		foreach(GameObject obj in objs){
			obj.SetActive(false);
		}
		objs[currentObj].SetActive(true);
	}
	
	void undoAction(){
		Debug.Log("undoAction");

		
		if(RaceEnded) //nova pista
		{
			Debug.Log ("New Track");
			Application.LoadLevel(0);
		}else
		if(showTextAskPlay == false)
		{
			if(addedObjs.Count > 0){
				Debug.Log ("removing obj");
				int lastId = addedObjs.Count-1;
				addedObjs[lastId].SetActive(false);
				GameObject.Destroy(addedObjs[lastId]);
				
				addedObjs.RemoveAt(lastId);
			}
		}
		else 
		{
			if(addedObjs.Count > 0){
				//Debug.Log ("removing obj");
				int lastId = addedObjs.Count-1;
				addedObjs[lastId].SetActive(false);
				GameObject.Destroy(addedObjs[lastId]);
				
				addedObjs.RemoveAt(lastId);
			}

			textAskPlay.SetActive(false);
			showTextAskPlay = false;

			ReadyForRace = false;
			RaceReady.SetActive(false);
		}
	}
	
	void okAction(){
		Debug.Log ("OK action");

		if(RaceEnded) //reiniciar a corrida
		{
			Debug.Log ("Restart Race");
			ResetCars();
		}else
		if(showTextAskPlay == false)
		{
			GameObject obj = (GameObject)Instantiate(objs[currentObj]);
			obj.transform.parent = tracks.transform;
			obj.transform.position = objs[currentObj].transform.position;
			obj.transform.rotation = objs[currentObj].transform.rotation;
			addedObjs.Add(obj);
			if(obj.name.StartsWith("estrada_meta")){
				finalObjs.SetActive(false);
				ReadyForRace = true;
				RaceReady.SetActive(true);

				Debug.Log ("Chega Aqui");
				textAskPlay.SetActive(true);
				showTextAskPlay = true;
			}
		}else
		{
			Debug.Log ("Chega Aqui2");
			textAskPlay.SetActive(false);
			showTextAskPlay = false;
			countdownBool = true;

			showKilometersBlue.SetActive(true);
			showKilometersRed.SetActive(true);

			F_Blue.SetActive(true);
			R_Blue.SetActive(true);
			F_Red.SetActive(true);
			R_Red.SetActive(true);

			foreach(GameObject o in myCars)
			{
				o.SetActive(true);
			}
			
		}
		
	}
	
	public void ResetCars(){
		for(int i =0;i<myCars.Length;i++){
			myCars[i].transform.position = carsInitPosition[i];
			myCars[i].transform.rotation = carsInitRotation[i];
		}
		finalObjs.SetActive(false);
		ReadyForRace = true;
		RaceReady.SetActive(true);
		
		
		textAskPlay.SetActive(true);
		showTextAskPlay = true;
		RaceEnded = false;
		hideEndMenu();
	}
	
	public void FinishRace(int winner){
		RaceEnded = true;

		RaceReady.SetActive(false);
		cronometrS.stopTime();
		
		foreach(GameObject o in myCars)
		{
			o.rigidbody.velocity = Vector3.zero;
			o.rigidbody.angularVelocity = Vector3.zero;
		}
		
		string raceTime = cronometrS.getTimeString();

		showFinalTime.guiText.text = raceTime;
		showFinalTime.SetActive(true);

		textRestartRace.SetActive(true);
		textNewTrack.SetActive(true);
		//textExit.SetActive(true);

		showKilometersBlue.SetActive(false);
		showKilometersRed.SetActive(false);

		F_Blue.SetActive(false);
		R_Blue.SetActive(false);
		F_Red.SetActive(false);
		R_Red.SetActive(false);
		
		if(winner == 0){
			showBlueWinner.SetActive(true);
		}else{
			showRedWinner.SetActive(true);
		}
	}
	
	public void hideEndMenu(){
		showFinalTime.SetActive(false);
		
		textRestartRace.SetActive(false);
		textNewTrack.SetActive(false);
		//textExit.SetActive(false);
		showRedWinner.SetActive(false);
		showBlueWinner.SetActive(false);
	}
}
