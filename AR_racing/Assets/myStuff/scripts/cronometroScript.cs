﻿using UnityEngine;
using System.Collections;

public class cronometroScript : MonoBehaviour {
	public GameObject target;
	public bool counting;
	private float startTime;
	private float elapsedTime;
	private GUIStyle mainStyle = new GUIStyle();
	private GUIStyle backStyle = new GUIStyle();

	public Font font_digital;

	// Init
	void Start () {
		counting = false;
		startTime = 0;
		elapsedTime = 0;
		mainStyle.normal.textColor = Color.yellow;
		mainStyle.font = font_digital;
		mainStyle.fontSize = 30;
		mainStyle.fontStyle = FontStyle.Normal;
		backStyle.normal.textColor = Color.black;
		backStyle.font = font_digital;
		backStyle.fontSize = 31;
		backStyle.fontStyle = FontStyle.Normal;
	}
	// Updates time
	void Update () {
		/*if ((target.renderer.enabled) && (!(counting))) {
			restartTime();
		}*/
		if (counting) {
			elapsedTime = Time.time - startTime;
		}
	}
	// Prints on GUI
	void OnGUI(){ 
		if (counting) {
			GUI.Label(new Rect(((Screen.width/2) - 50), 0, 100, 20), getTimeString(),backStyle); 
			GUI.Label(new Rect(((Screen.width/2) - 50), 0, 100, 20), getTimeString(),mainStyle); 
		}
	}
	
	public string getTimeString() {
		float time = getTime ();
		string result = string.Concat(Mathf.Floor(time / 60),"'");
		if ((time % 60) < 10) {
			result = result + "0";
		}
		result = string.Concat(result,Mathf.Floor(time % 60),"''");
		if (Mathf.Floor ((time % 1) * 1000) < 10) {
			result = result + "00";
		} else if (Mathf.Floor ((time % 1) * 1000) < 100) {
			result = result + "0";
		}
		return string.Concat(result,Mathf.Floor ((time % 1) * 1000));
	}
	
	public float getTime() {
		return elapsedTime;
	}
	
	public void restartTime() {
		startTime = Time.time;
		counting = true;
	}
	
	public void stopTime() {
		counting = false;
	}
}