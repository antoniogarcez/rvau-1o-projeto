﻿using UnityEngine;
using System.Collections;

public class buttonHandler : MonoBehaviour, IVirtualButtonEventHandler {

	public GameObject[] selectObjs;
	public GameObject[] finalObjs;
	public GameObject[] imgButtons;
	public GameObject referencial;
	
	private double pressTime = 1.0;
	private double curPressTime = 0;
	private double curPressTimeOk = 0;
	private bool pressing = false;
	private bool pressingOk = false;
	private int curObj = 0;
	
	private bool locked = false;

	// Use this for initialization
	void Start () {
	
		// Search for all Children from this ImageTarget with type VirtualButtonBehaviour
		VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
		for (int i = 0; i < vbs.Length; ++i) {
			// Register with the virtual buttons TrackableBehaviour
			vbs[i].RegisterEventHandler(this);
		}
		
		foreach(GameObject obj in selectObjs){
			
			obj.SetActive(false);
		}
		foreach(GameObject obj in finalObjs){
			
			obj.SetActive(false);
		}
		selectObjs[0].SetActive(true);
		
	}
	
	void Update(){
		if(!locked || true){
			if(pressing){
				curPressTime += Time.deltaTime;
				if(curPressTime > pressTime){
					curPressTime = 0;
					pressButton();
				}
			}else{
				curPressTime = 0;
			}
			
			if(pressingOk){
				curPressTimeOk += Time.deltaTime;
				if(curPressTimeOk > pressTime){
					curPressTimeOk = 0;
					pressButtonOk();
				}
			}else{
				curPressTimeOk = 0;
			}
		}
		
		
	}
	
	public void pressButton(){
		curObj++;
		if(curObj >= selectObjs.Length){
			curObj = 0;
		}
		foreach(GameObject obj in selectObjs){
			obj.SetActive(false);
		}
		selectObjs[curObj].SetActive(true);
		
		pressing = false;
	}
	
	public void pressButtonOk(){
		
		if(!locked){
		
			foreach(GameObject obj in selectObjs){
				obj.SetActive(false);
			}
			finalObjs[curObj].SetActive(true);
			
			/*foreach(GameObject obj in imgButtons){
				obj.SetActive(false);
			}*/
			
			pressingOk = false;
			locked = true;
		}else{
			
			GameObject objs = (GameObject)Instantiate(finalObjs[curObj]);
			objs.transform.parent = referencial.transform;
			locked = false;
			foreach(GameObject obj in finalObjs){
				obj.SetActive(false);
			}
			selectObjs[0].SetActive(true);
			curObj = 0;
			
		}
		
	}
	
	/// <summary>
	/// Called when the virtual button has just been pressed:
	/// </summary>
	public void OnButtonPressed(VirtualButtonAbstractBehaviour vb) { 
		if(vb.name.Equals("myBtnNext")){
			pressing = true;
			pressingOk = false;
		}else
		if(vb.name.Equals("myBtnOk")){
			pressingOk = true;
			pressing = false;
		}
		
		//firstObj.SetActive(false);
		//secondObj.SetActive(true);
	}
	
	/// <summary>
	/// Called when the virtual button has just been released:
	/// </summary>
	public void OnButtonReleased(VirtualButtonAbstractBehaviour vb) { 
		if(vb.name.Equals("myBtnNext")){
			pressing = false;
		}else
		if(vb.name.Equals("myBtnOk")){
			pressingOk = false;
		}
		//firstObj.SetActive(true);
		//secondObj.SetActive(false);
	}
}
