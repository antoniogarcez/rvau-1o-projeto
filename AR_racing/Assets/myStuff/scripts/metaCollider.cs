﻿using UnityEngine;
using System.Collections;

public class metaCollider : MonoBehaviour {
	
	refSelector myScript;
	// Use this for initialization
	void Start () {
		myScript = GameObject.Find("ImageTarget referencial").GetComponent<refSelector>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other){
		Debug.Log("passou: "+other.name);
		if(other.transform.parent.name.StartsWith("AustinMartin1")){
			myScript.FinishRace(0);
		}else if(other.transform.parent.name.StartsWith("AustinMartin2")){
			myScript.FinishRace(1);
		}
	}
}
