﻿using UnityEngine;
using System.Collections;

public class selectorCollider : MonoBehaviour {

	public GameObject selector;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other) {
		Debug.Log("colliding to "+other.name);
		if(isTracks(other.gameObject)){
			Quaternion qt = other.transform.rotation;
			qt.y = selector.transform.rotation.y;
			selector.transform.rotation = qt;
			//selector.transform.Translate(0,other.transform.position.y-selector.transform.position.y,0);
			Debug.Log("rotating");
		}
	}
	
	bool isTracks(GameObject ob){
		Transform t = ob.transform;
		while(t.parent != null){
			if(t.parent.name.Equals("tracks")){
				return true;
			}else{
				t = t.parent;
			}
		}
		return false;
	}
}
