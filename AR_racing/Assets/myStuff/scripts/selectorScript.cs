﻿using UnityEngine;
using System.Collections;

public class selectorScript : MonoBehaviour {

	public GameObject Next;
	public GameObject Ok;
	public GameObject Ref;
	public GameObject[] tracks;
	
	
	private bool isMarkerVisible = false;
	private int curTrack = 0;
	
	
	
	// Use this for initialization
	void Start () {
		foreach(GameObject obj in tracks){
			obj.SetActive(false);
		}
		tracks[curTrack].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		if(isMarkerVisible){
			if(!Next.renderer.enabled && !Ok.renderer.enabled){
				isMarkerVisible = false;
			}
		}else{
			if(Next.renderer.enabled){
				isMarkerVisible = true;
				nextAction();
			}else if(Ok.renderer.enabled){
				isMarkerVisible = true;
				okAction();
			}
		}
	}
	
	void nextAction(){
		curTrack++;
		if(curTrack >= tracks.Length){
			curTrack = 0;
		}
		foreach(GameObject obj in tracks){
			obj.SetActive(false);
		}
		tracks[curTrack].SetActive(true);
	}
	
	void okAction(){
		GameObject obj = (GameObject)Instantiate(tracks[curTrack]);
		obj.transform.parent = Ref.transform;
		obj.transform.position = tracks[curTrack].transform.position;
		obj.transform.rotation = tracks[curTrack].transform.rotation;
	}
}
